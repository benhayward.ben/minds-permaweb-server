/**
 * Minds Arweave Server - acts as a bridge to the Arweave blockchain.
 * 
 * Set up add the following to a .env file.
 *
 * ARWEAVE_XPRIV - required - your private key.
 * DEBUG - optional - set to true for debugging output.
 * MAX_BYTES=10000000 - mandatory - max upload size in bytes.
 * 
 * Endpoints:
 * 
 * POST to /permaweb/getId - Dry run transaction to get ID.
 * POST to /permaweb - Generate validate and dispatch a transaction from params.
 * GET from /permaweb/:txid - Get a transaction by id.
 * GET / - health check
 * 
 * @author Ben Hayward
 */
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const FileType = require('file-type');
const app = express();
const port = process.env.PORT || 5000;
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.json()); // any request coming in, transfer all body into JSON
app.use(cors()) // allow cross origin from client localhost
require('dotenv').config();
const Arweave = require('arweave/node');

// Remote
const arweave = Arweave.init({
  host: 'arweave.net',// Hostname or IP address for a Arweave host
  port: 443,          // Port
  protocol: 'https',  // Network protocol http or https
  timeout: 20000,     // Network request timeouts in millisecondsCannot read property 'replace' of undefined
  logging: false,     // Enable network request logging
});

// // Local
// const arweave = Arweave.init({
//     host: '127.0.0.1',
//     port: 1984,
//     protocol: 'http'
// });

/**
 * Gets the ID for a transaction by dry-running and not sending.
 * @param { string } text - text content
 * @param { string } guid - user guid to store as tag
 * @param { string } mindsLink - link back to Minds to store as tag
 * @param { string } thumbnailSrc - thumbnailSrc
 */
app.post('/permaweb/getId', async(req, res) => {
  try {
    const text = req.body.text,
      guid = req.body.guid,
      mindsLink = req.body.minds_link || null,
      thumbnailSrc = req.body.thumbnail_src || null;

    if (!guid || !mindsLink) {
      return res.json({
        status: 400,
        message: 'You must pass the data and guid parameters',
      })
    }
  
    const tx = await generateSignedTransaction(text, guid, mindsLink, thumbnailSrc);

    // Check proposed data size against maximum length in bytes.
    if (Number(tx.data_size) > Number(process.env.MAX_BYTES)) {
      return res.json({
        status: 400,
        message: `Exceeded maximum of ${process.env.MAX_BYTES} bytes`,
      });
    }

    return res.json({
      status: 200,
      id: tx.id,
    });

  } catch(e) {
    console.error(e);
    res.json({ status: 500, message: e })
  }
});

/**
 * Generates and submits a transaction to the permaweb from input params.
 * @param { string } text - text content
 * @param { string } guid - user guid to store as tag
 * @param { string } mindsLink - link back to Minds to store as tag
 * @param { string } thumbnailSrc - thumbnailSrc
 * @returns { Object } - status, message (on fail) and id (on success) params . 
 */
app.post('/permaweb', async(req, res) => {
  const text = req.body.text,
    guid = req.body.guid,
    mindsLink = req.body.minds_link,
    thumbnailSrc = req.body.thumbnail_src || null;

  try {
    if (!guid || !mindsLink) {
      return res.json({
        status: 400,
        message: 'You must pass the text, guid, minds_link and thumbnail_src parameters',
      })
    }
  
    const tx = await generateSignedTransaction(text, guid, mindsLink, thumbnailSrc);

    debug(tx);

    // Check proposed data size against maximum length in bytes.
    if (Number(tx.data_size) > Number(process.env.MAX_BYTES)) {
      return res.json({
        status: 400,
        message: `Exceeded maximum of ${process.env.MAX_BYTES} bytes`,
      });
    }
    
    const uploader = await arweave.transactions.getUploader(tx);

    while (!uploader.isComplete) {
      await uploader.uploadChunk();
      debug(`${uploader.pctComplete}% complete, ${uploader.uploadedChunks}/${uploader.totalChunks}`);
    }

    return res.json({
      status: uploader.lastResponseStatus,
      id: tx.id,
    });

  } catch(e) {
    console.error(e);
    res.json({ status: 500, message: e })
  }
});

/**
 * Get a transaction from the permaweb by id
 * @param { string } txid - transaction id to check.
 * @returns { Object } data from transaction
 */
app.get('/permaweb/:txid', async(req, res) => {
  try {
    const txid = req.params.txid;

    // Get the data decode as string data
    const data = await arweave.transactions.getData(txid, {decode: true, string: true})

    if (Object.keys(data).length === 0) {
      return res.json({
        status: 404,
        message: 'Content not found.'
       });
    }

    return res.json({
      status: 200,
      data: data,
    });
  } catch(e) {
    console.error(e);
    res.json({
      'status': 500,
      'message': e, 
    });
  }
});

/**
 * Health check endpoint
 * @returns { Object } status of 200, readyState of 1.
 */
app.get('/', (req, res) => {
  res.send({
    status: 200,
    readyState: 1,
  });
});

/**
 * Sets app to listen on set port.
 */
app.listen(port, error => {
  if (error) throw error
  debug('Server running on port ' + port)
});

/**
 * Generates and validates a signed transaction object.
 * @param {string} text 
 * @param {string} guid 
 * @param {string} mindsLink 
 * @param {string} thumbnailSrc 
 * @returns { Object } - transaction object.
 */
const generateSignedTransaction = async(text, guid, mindsLink, thumbnailSrc) => {
  let response, mimeType;

  if  (thumbnailSrc) {
    try {
      response = await axios.get(thumbnailSrc,  { responseType: 'arraybuffer' })
      fileType = await FileType.fromBuffer(response.data);
    
      if(fileType.mime.startsWith('image/')) {
        mimeType = fileType.mime;
      } else {
        response = null; // if invalid still post with minds link to content.
      }
    } catch(e) {
      debug(`An error occurred adding image with the url ${thumbnailSrc}`, e);
    }
  }
  
  tx = await arweave.createTransaction({
    data: thumbnailSrc && response && response.data ? response.data : text
  }, JSON.parse(process.env.ARWEAVE_XPRIV));  

  if (mimeType) {
    tx.addTag('Content-Type', mimeType);
    tx.addTag('minds:text', text);
  }

  tx.addTag('minds:guid', guid);
  tx.addTag('minds:link', mindsLink);

  // sign it
  await arweave.transactions.sign(tx, JSON.parse(process.env.ARWEAVE_XPRIV));

  return tx;
}

/**
 * Debug utility. Fires only if env for debug is set to true.
 * @param  {...any} content 
 */
const debug = (...content) => {
  if (process.env.DEBUG) {
    console.log(...content);
  }
}
